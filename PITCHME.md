---
@snap[west headline]
## Spring Framework Introduction
@snapend

@snap[south-west byline]
A **REALLY** Quick Introduction into the Spring Framework
@snapend
---
### What will we look at?
1. Quick Introduction
1. What is a Spring Bean?
1. What is Spring's Application Context?
1. 4 Annotations
1. Best practices
1. What is next?
---
### First: Always remember
** Using the Spring Framework is easy **
---
### Spring Framework Origin?    
   * Java2EE was Big Bulky, Complicated, and Expensive. 
   * Rob Johnson: Expert One-on-One J2EE Development without EJB.
   * Bases for the Framework.
---
### Spring Framework: Characteristics    
   * Opensource
   * Lightweight ( Relative )
   * NOT A replacement for Java2EE/JavaEE/Jakarta EE
   * It is an "Eco System".
---
### Spring Framework promotes:
**"Dry Socs?"**
---
### Do not repeat yourself.
* Removes boilerplate code
* But also applies to your code
---
### Separation of concerns
* Config vs Bussiness logic
* Config changes between environments
---    
### What is a **Spring Bean**?
Just an object managed by the Spring Framework. 
---
### Managed by Spring?
In short, we just "ask" Spring to keep track of Java objects for us. 
---
### Spring's Application Context

* To keep track of Spring beans, Spring uses an **Application Context**.
* "Application Context" - It is just another name for a HashMap.
---
### Application Context
When Spring starts up, it collects metadata of all beans (This metadata is called Bean Definitions). This metadata is used to create the actual beans and stores them in the Application context.
---
### Application Context
During runtime, our application interacts with this application context.
Or in other words, our application adds beans and retrieve beans from the application context.
---
### 4 Annotations to make you a Spring Developer :)
1. @Configuration
1. @Bean
1. @Component
1. @Autowire
---
** How to store beans to the application context? **
---
### @Configuration + @Bean

```
@Configuration
class ApplicationConfiguration {
	
	@Bean
	public ApplicationService applicationService() {
		return new ApplicationService();
	}
	
}
```
---
### @Component
```
@Component
class ApplicationService {
	// .. 
}
```
---
** How to retrieve Beans from the Application Context? **
---
### @Autowire
* Fields
```
@Autowired
private PersonRepository personRepository;
```
* Constructors
```
@Autowired
public ApplicationService(PersonRepository personRepository) {
	this.personRepository = personRepository;
	/// ...
```
* Setters
```
@Autowired
public void setPersonRepository(PersonRepository personRepository) {
	this.personRepository = personRepository;
}
```
---
### Best practices
1. If you are accessing the Application Context programmatically, you are doing it wrong*.
1. When to use @Configuration + @Bean vs @Component
1. @Bean in a @Component?
1. Where to use @Autowire?
1. Hey? Where did my @Autowire go?

---
### Always remember 
** Using Spring is easy **
